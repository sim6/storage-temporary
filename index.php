<?php 
/*
 * This file is part of Magatzem Temporal
 *
 * Copyright (C) 2006 Simó Albert i Beltran
 * 
 * Magatzem Temporal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Magatzem Temporal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Magatzem Temporal.  If not, see <http://www.gnu.org/licenses/>.
 *
 * http://www.gnu.org/licenses/agpl.txt
 *
 * Version 0.0.1
 */

// Help URL
$url_help="http://probeta.net/dokuwiki/doku.php?id=guies_dels_serveis:magatzem_temporal";

// Data URL
$url_data="https://" . $_SERVER['SERVER_NAME'] . "/m/d/";

// Storage directory
$dir="/var/www/magatzem/data";


// Maxium storage in bytes.
$max_storage=2048000;

//natejem el magatzem.
if ($dh = opendir($dir))
{
	while (($file = readdir($dh)) !== false)
	{
		if(is_file($dir."/".$file))
		{
			if($file != "." && $file != ".." && ereg("^m_",$file))
			{
				//if(time()-(60*60*24*31*2) >= filectime($dir."/".$file))
				if(time()-(60*60*24*31*2) >= fileatime($dir."/".$file))
				{
					unlink($dir."/".$file);
				}
			}
   		}
  	}
	closedir($dh);
}

if ( `du -s $dir | cut -f1` >= $max_storage )
{
	$output = 'Aquest <a href="' . $url_help . '">magatzem temporal</a> està fora de servei.';
}
else
{

	$output = '';

	if (isset($_GET['expired']))
	{
		$output .= '<p class="error">El contingut ha caducat, aquest <a href="' . $url_help . '">magatzem temporal</a> nom&eacute;s mant&eacute; el contingut durant 2 messos.</p>';
	}

	$output .= file_get_contents('form.html');

    /* This code is for attachments */
    if ((bool) ini_get('file_uploads')) {

        /* Calculate the max size for an uploaded file.
         * This is advisory for the user because we can't actually prevent
         * people to upload too large files. */
        $sizes = array();
$si = array("T","G","M","K");
$mul = array("000000000000","000000000", "000000", "000");
        /* php.ini vars which influence the max for uploads */
        $configvars = array('post_max_size', 'memory_limit', 'upload_max_filesize');
        foreach($configvars as $var) {
            /* skip 0 or empty values, and -1 which means 'unlimited' */
$sizes[] = str_replace($si,$mul,ini_get($var));
/*
            if( $size = getByteSize(ini_get($var)) ) {
                if ( $size != '-1' ) {
                    $sizes[] = $size;
                }
            }
*/
        }

        if(count($sizes) > 0) {
//            $maxsize_text = '(max.&nbsp;' . show_readable_size( min( $sizes ) ) . ')';

            $maxsize_text = '<p>(max.&nbsp;' . str_replace($mul,$si,min( $sizes )) . ')</p>';
//            $maxsize_input = addHidden('MAX_FILE_SIZE', min( $sizes ));
        } else {
            $maxsize_text = $maxsize_input = '';
        }
    }
    $output .= $maxsize_text;
	
	if (isset($_FILES['userfile']) && is_uploaded_file($_FILES['userfile']['tmp_name'])) {
		//escrivim la ruta completa que tindrà el fitxer pujat
		$fitxer="m_".date('mdHis').rand('100','999')."_".$_FILES['userfile']['name']; 
		//si pujem un fitxer php.foo executa el php, igual amb perl i altres.
		//[ToDo] mirar quines extensions més executa l'apache.
		$fitxer = str_ireplace(".php", ".php.txt", $fitxer);
		$fitxer = str_ireplace(".pl", ".pl.txt", $fitxer);
		//substituir caracters no alfanumerics per guions baixos per millorar que l'enllaç proporcionat (els espais donen problemes amb enllaços enviats per correu electronic, idem per ç, accents, {}, []...
		$fitxer = preg_replace("/[^0-9a-zA-Z_\.-]/","_",$fitxer);
		$ruta=$dir."/".$fitxer;   

		//control errors
		if(file_exists($ruta))
		{
			$output .='<p class="error">Canvia el nom del fitxer per poder-lo pujar.</p>';
		}
		else
		{
			//fitxer de log
			if(!$handle = fopen("/var/log/magatzem.log", "a+"))
			{
				die("Error: no puc obrir fitxer de log");
			}
			$log=date('Y-m-d H:i:s')." --> ".$ruta."\n";
			if(!fwrite($handle,$log))
			{
				die("Error: no puc escriure al fitxer de log");
			}
			if(!fclose($handle))
			{
				die("Error: no puc tancar el fitxer");
			}

			//copiem el fitxer a la ubicació demanada.
			if(!copy($_FILES['userfile']['tmp_name'], $ruta))
			{
				die("No hi ha permisos per copiar al directori $ruta");
			}
			$output .= "<div align='center'><p>El contingut s'ha pujat correctament!!</p><p style='border: 1px dotted grey;padding: 1em; margin: 1em 10%;'>Document accessible fins el ".date('d-m-Y', strtotime('+2 month'))." en <a href='" . $url_data . $fitxer."'>aquest enlla&ccedil;</a>:<br/> " . $url_data . $fitxer."</p></div>\n";
		}
	}
}

include('template.php');

?>
